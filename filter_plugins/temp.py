def inventory_to_hostvars(hosts, hostvars):
    """
    This function returns a list of variables such as hostname,host ip from the inventory hostvars
    """
    result = []
    for host in hosts:

        name = hostvars[host].get("inventory_hostname_short")
        fqdn = hostvars[host].get("inventory_hostname")
        zfs_pool = hostvars[host].get("rsync_module_backup_pool")
        path = zfs_pool + "/" + name
        result.append({"name": name, "path": path, "hosts_allow": fqdn})
    return result


class FilterModule(object):
    def filters(self):
        return {"inventory_to_hostvars": inventory_to_hostvars}
